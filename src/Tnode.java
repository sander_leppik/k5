import java.util.*;

public class Tnode {

   private String name;
   private Tnode firstChild;
   private Tnode nextSibling;


   Tnode(String name){
      this.name = name;
   }

   @Override
   public String toString() {
      StringBuffer b = new StringBuffer();
      b.append(name);
      if(!isLeaf()){
         b.append("(");
         b.append(firstChild.toString());
         b.append(",");
         b.append(firstChild.nextSibling.toString());
         b.append(")");
      }
       return b.toString();
   }
//   @Override
//   public String toString(){
//      String str = name;
//      if(!isLeaf()){
//         str += "(" + firstChild.toString() + "," +firstChild.nextSibling.toString() + ")";
//      }
//
//      return str;
//   }

   public static Tnode buildFromRPN (String pol) {
      String[] strArr = pol.trim().split("\\s+");
      Stack<Tnode> stack = new Stack<>();

      for(int i = 0; i < strArr.length; i++){
           if(isOperator(strArr[i])){
            String tempName = strArr[i];

            try {
               //Throwing an error if there are not enough elements in the stack
               Tnode tempTnodeFirst = stack.pop();
               Tnode tempTnodeSecond = stack.pop();

               if(strArr[i].equals("SWAP")){
                  stack.push(tempTnodeFirst);
                  stack.push(tempTnodeSecond);

               }else if(strArr[i].equals("ROT")){
                  if(stack.isEmpty()){
                     throw new RuntimeException("Third number missing in ROT operation: " + pol);
                  }
                  Tnode tempTnodeThird = stack.pop();
                  stack.push(tempTnodeSecond);
                  stack.push(tempTnodeFirst);
                  stack.push(tempTnodeThird);
               }else{
                  //Relate Tnodes to eachother(Tnode - sibling Tnode)
                  tempTnodeSecond.addSibling(tempTnodeFirst);
                  //Make a new Tnode and add a child
                  Tnode tempTnode = new Tnode(tempName);
                  tempTnode.addChild(tempTnodeSecond);

                  //Push Tnode with a child(witch has a sibling) to stack
                  stack.push(tempTnode);
               }
            } catch (EmptyStackException e) {
               throw new RuntimeException("Numbers missing in: " + pol);
            }
         } else {
            try {
               //Check if is a number, otherwise throw an error
               double tempNumber = Double.parseDouble(strArr[i]);

               //Make into Tnode and push to stack
               Tnode tempNode = new Tnode(strArr[i]);
               stack.push(tempNode);
            } catch (RuntimeException e) {
               throw new RuntimeException("Illegal symbol: " + strArr[i] + " in " + pol);
            }
         }
      }

      //Pop root Tnode
      Tnode root = stack.pop();

      //If theres still Tnodes left something went wrong
      if(stack.isEmpty()){
         return root;
      }else{
         throw new RuntimeException("Too many numbers in: " + pol);
      }
   }

   public static boolean isOperator(String s){
      String[] operators = {"+", "-", "*", "/", "SWAP", "ROT"};
      for (String operator:operators) {
         if(operator.equals(s)){
            return true;
         }
      }
      return false;
   }

   public void addChild(Tnode child) {
      this.firstChild = child;
   }

   public void addSibling(Tnode sibling){
      this.nextSibling = sibling;
   }

   public boolean isLeaf(){
      return this.firstChild == null;
   }

   public static void main (String[] param) {
      String s = "2 9 ROT + SWAP -";
      System.out.println ("RPN: " + s);
      Tnode res = buildFromRPN (s);
      System.out.println ("Tree: " + res);
   }
}
